<h1>PEC1 - The Secret of Monkey Island (Combate de Insultos Edition)</h1>
<p>Ésta práctica es el diseño e implementación de un duelo de insultos en forma de videojuego. Actua como analogía del duelo de insultos del clásico juego The Secret of Monkey Island de Electronic Arts</p>

<h2>Implementación y estructuración</h2>
<p>Éste juego se ha implementado con Unity en la versión 2018.2.14.</p>
<p>Se estructura en 3 escenas: Menu principal, juego y final. Los assets del proyecto se dividen en:</p>
<h3>Scenes</h3>
<p>Donde hay las tres escenas del juego.</p>
<h3>Audio</h3>
<p>Los 4 archivos de audio: el del menu principal, el del juego y dos opciones para el final dependiendo de quien gane la partida.</p>
<h3>Fonts</h3>
<p>Fuente tipográfica utilizada</p>
<h3>Scripts</h3>
<p>Los diferents scripts utilizados están comentados en el propio código. Principalmente son para configurar el comportamiento de algunos botones, la estructura de datos, el funcionamiento lógico del juego y la implementación de la escena final.</p>
<p>Principalmente se inicia la escena de juego con todos los contadores a 0 y dependiendo del turno se cargan insultos o respuestas des de un archivo JASON. A partir de aquí se inicia un bucle de asaltos hasta que alguien gana 3 y finaliza la partida.
<h3>Sreaming Assets</h3>
<p>Donde se localiza el archivo JSON con los datos de los insultos y las respuestas.</p>
<h3>Prefabs</h3>
<p>Donde se situa el único prefab del juego que actua como botón de opciones.</p>
<h3>Sprites</h3>
<p>Las imágenes utilizadas de los fondos y personajes de la escena del juego.</p>


<h2>Como jugar</h2>
<p>Para jugar hay que clicar a "nueva partida". En la pantalla de juego se dará el turno al jugador o al adversario de forma aleatoria. Si es el turno del jugador, tendrá que escoger un insulto a gritar de la lista y el adversario intentará responder al instante. Si es el turno del adversario, el jugador tendrá que escoger la respuesta correcta al insulto de entre la opciones que aparecen en pantalla.</p>
<p>Quien gane el asalto obtiene el turno en el siguiente asalto. El que gane 3 asaltos o rounds gana la partida.</p>

<p>link del gameplay: https://vimeo.com/301470639</p>

