﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioEnd : MonoBehaviour 
{
    public AudioClip audioWin;
    public AudioClip audioLose;

    void Awake()
    {
        // Si gana el jugador se activa el audio del ganador y si no el del perdedor.
        if(GameplayManager.ganaJugador)
        {
            AudioSource.PlayClipAtPoint(audioWin, new Vector3(0 ,0 ,-10 ));
        }else{
            AudioSource.PlayClipAtPoint(audioLose, new Vector3(0, 0, -6));
        }
    }
}
