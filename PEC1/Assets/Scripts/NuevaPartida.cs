﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // <-- Esta librería es necesaria para poder usar directamente SceneManager

public class NuevaPartida : MonoBehaviour {

    public void CargarPartidaNueva()
    {
        SceneManager.LoadScene("Game");
    }
}
