﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
// Clase con las variables de una pregunta y su respuesta
public class QuestionData
{
    public string questionText;
    public string answerText;
}