﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class GameplayManager : MonoBehaviour
{
    public Text adversarioDisplayText;
    private QuestionData[] allQuestions;
    public Transform answersParent;
    public GameObject buttonPrefab;
    public Text roundDisplayText;
    public Text jugadorScoreDisplayText;
    public Text adversarioScoreDisplayText;
    public Text turnoDisplayText;
    public static string conclusion;
    public static bool ganador;
    public static bool ganaJugador;

    private bool turnoJugador;
    private int questionIndex;
    private int round;
    private int scoreJugador;
    private int scoreAdversario;
    private string gameDataFileName = "data.json";

    void Start()
    {
        // Asignamos de forma aleatoria si es el jugador que tiene el turno
        turnoJugador = (Random.value > 0.5f);

        // Inicializamos los contadores de turno y puntuación
        round = 1;
        scoreJugador = 0;
        scoreAdversario = 0;
        ganador = false;

        // Cargamos los datos del juego
        LoadGameData();
        // Mostramos los datos iniciales
        ShowData();

    }

    // Función encargada de mostrar los datos actualizados de cada asalto
    private void ShowData()
    {
        // Actualizamos marcadores
        roundDisplayText.text = "Round: " + round.ToString();
        jugadorScoreDisplayText.text = "" + scoreJugador.ToString();
        adversarioScoreDisplayText.text = "" + scoreAdversario.ToString();
        turnoDisplayText.text = turnoJugador ? "Empiezas tu" : "Te toca responder";

        // Borramos los anteriores botones
        foreach (Transform child in answersParent.transform)
        {
            Destroy(child.gameObject);
        }
        // Colocamos los nuevos botones
        // Por cada opción crearemos un botón
        for (int i = 0; i < allQuestions.Length; i++)
        {
            // Creamos el botón y lo asociamos al elemento de la UI correspondiente.
            GameObject buttonOptionCopy = Instantiate(buttonPrefab);
            buttonOptionCopy.transform.SetParent(answersParent);

            // Asociamos
            FillListener(buttonOptionCopy.GetComponent<Button>(), i);

            // Rellenamos el texto del botón con pregunta o respuesta dependiendo de quien tenga el turno
            buttonOptionCopy.GetComponent<Text>().text = turnoJugador ? allQuestions[i].questionText : allQuestions[i].answerText;
        }

        // Asignamos un valor aleatorio de pregunta
        questionIndex = Random.Range(0, allQuestions.Length);

        adversarioDisplayText.text = turnoJugador ? "" : allQuestions[questionIndex].questionText;
    }

    /// Esta función asocia el hacer click en el botón, con la función 'ButtonClicked'.
    /// <param name="button">Botón sobre el que se añadirá la acción.</param>
    /// <param name="index">posición del botón dentro de la lista de respuestas.</param>
    void FillListener(Button button, int index)
    {
       button.onClick.AddListener(
            () => {
                ButtonClicked(index);
            }
            );
    }

    // Función de acciones al pulsar el botón
    public void ButtonClicked(int index)
    {
        string answer;
        string correctAnswer;

        // Comparamos si la respuesta es correcta
        answer = allQuestions[questionIndex].answerText;
        correctAnswer = allQuestions[index].answerText;

        // Si la respuesta es correcta, dependiendo de quien tenga el turno gana o pierde el asalto
        if (answer == correctAnswer)
        {
            if (turnoJugador)
            {
                scoreAdversario++;
            }
            else
            {
                scoreJugador++;
                turnoJugador = true;
            }
        }
        else
        {
            if (turnoJugador)
            {
                scoreJugador++;
                turnoJugador = true;
            }
            else
            {
                scoreAdversario++;
            }
        }

        EndRound();
        ShowData();

    }

    // Función para comprovar si alguien gana 3 asaltos. Si se da el caso guarda el ganador y carga la pantalla final
    public void EndRound()
    {
        ganador |= scoreJugador == 3;
        ganador |= scoreAdversario == 3;

        if (ganador)
        {
            ganaJugador = scoreJugador == 3 ? true : false;
            SceneManager.LoadScene("Final");
        }else
        {
            round++;
        }
    }

    // Función que carga los datos de las preguntas a partir de un documento JSON
    private void LoadGameData()
    {
        // Asignamos el directorio del archivo a cargar
        string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);

        // Si hay el archivo en el directorio lo cargamos
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
            allQuestions = loadedData.allQuestions;
        }
        else
        {
            Debug.LogError("No se pueden cargar los datos del juego");
        }
    }
}