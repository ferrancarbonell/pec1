﻿using UnityEngine;
using UnityEngine.UI;

public class WinnerLoser : MonoBehaviour {

    public Text winnerLoser;

    void Start ()
    {
        winnerLoser.text = GameplayManager.ganaJugador ? "Has ganado la partida" : "Has perdido la partida";
    }
}
